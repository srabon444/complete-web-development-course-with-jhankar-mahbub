const codingTime = () => {
    alert("Now is coding time. Start doing work!");
};

const goingToVacation = () => {
    const answerOptions = confirm('Do you want to go on a vacation');
    console.log(answerOptions);
};

const askNameFunc = () => {
    const askName = prompt('What is your name?');
    if (askName) {
        alert(askName + ' is a nice name!!');
        console.log(askName, ' is a nice name!!');
    }
};