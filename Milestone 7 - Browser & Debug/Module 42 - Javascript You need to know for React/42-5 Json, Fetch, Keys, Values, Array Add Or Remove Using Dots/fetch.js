const employee = {
    name: "Ashraful Islam",
    id: 482266,
    machine: 'Windows 10',
    language: ['Javascript', 'HTML', 'Java'],
    specifications: {
        ide: {
            name: 'Webstorm',
            company: 'Jetbrains',
            price: 120,
        },
        height: 5.7,
        weight: 58,
        favFood: 'Biriani',
    }
};

const employeeJSON = JSON.stringify(employee);
console.log(employee);
console.log(employeeJSON);

const employeeObj = JSON.parse(employeeJSON);
console.log(employeeObj);

// Fetch
fetch('url')
    .then(res => res.json())
    .then(data => console.log(data));

// Keys
const keys = Object.keys(employee);
const value = Object.values(employee);

console.log(keys);
console.log(value);

