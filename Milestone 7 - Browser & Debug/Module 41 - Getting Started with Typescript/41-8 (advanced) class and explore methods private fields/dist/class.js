class Car {
    constructor(model, price) {
        this.model = model;
        this.price = price;
        this._millage = 100000;
    }
    getActualMillage() {
        return this._millage + 50000;
    }
    getTotalPrice(tax) {
        const taxAMount = this.price * (tax / 100);
        return this.price + taxAMount;
    }
}
const tesla = new Car('Model X', 7600000);
const totalPrice = tesla.getTotalPrice(10);
console.log(totalPrice);
const millage = tesla.getActualMillage();
console.log(millage);
//# sourceMappingURL=class.js.map