import React from 'react';
import './Service.css';

const Service = ({service}) => {
    // const {service} = props;
    const {name, price, img, description, time} = service;
    return (
        <div className="service">
            <img src={img} alt=""/>
            <h3>Name: {name}</h3>
            <h5>Price: {price}</h5>
            <h4>Time: {time}</h4>
            <p className="px-5">Description: {description}</p>
        </div>
    );
};

export default Service;
