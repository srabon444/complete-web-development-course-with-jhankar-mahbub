import React from 'react';
import notFound from '../../images/404.jpg';
import {Link} from "react-router-dom";

const NotFound = () => {
    return (
        <div>
            <img style={{width: "100%"}} src={notFound} alt=""/>
            <Link to="/">
                <button className="btn-primary">Go Back</button>
            </Link>
        </div>
    );
};

export default NotFound;
