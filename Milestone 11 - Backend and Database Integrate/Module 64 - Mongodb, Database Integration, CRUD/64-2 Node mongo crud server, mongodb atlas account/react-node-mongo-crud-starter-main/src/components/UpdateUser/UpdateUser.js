import React, {useEffect, useState} from 'react';
import {useParams} from "react-router-dom";

const UpdateUser = () => {
    const [user, setUser] = useState({});
    const {id} = useParams();

    useEffect(() => {
        const url = `http://localhost:5000/users/${id}`;
        fetch(url)
            .then(res => res.json())
            .then(data => setUser(data));
    }, []);

    //Update User
    const updateNameChange = e => {
        const updatedName = e.target.value;
        const updatedUser = {name: updatedName, email: user.email};
        setUser(updatedUser);
    };

    const updateEmailChange = e => {
        const updatedEmail = e.target.value;
        const updatedUser = {name: user.name, email: updatedEmail};
        setUser(updatedUser);
    };

    const handleUpdateUser = e => {
        const url = `http://localhost:5000/users/${id}`;
        fetch(url, {
            method: 'PUT',
            headers: {
                'content-type': 'application/json'
            },
            body: JSON.stringify(user)
        })
            .then(res => res.json())
            .then(data => {
                if (data.modifiedCount > 0) {
                    alert('Updated Successfully');
                    setUser({});
                }
            })
        e.preventDefault();
    };

    return (
        <div>
            <h2>Update {user.name} {user.email}</h2>
            <p><small>{id}</small></p>
            <form onSubmit={handleUpdateUser}>
                <input type="text" onChange={updateNameChange} value={user.name || ''} placeholder="Updated Name"/>
                <input type="email" onChange={updateEmailChange} value={user.email || ''} placeholder="Updated Email"/>
                <input type="submit" value="Update"/>
            </form>
        </div>
    );
};

export default UpdateUser;