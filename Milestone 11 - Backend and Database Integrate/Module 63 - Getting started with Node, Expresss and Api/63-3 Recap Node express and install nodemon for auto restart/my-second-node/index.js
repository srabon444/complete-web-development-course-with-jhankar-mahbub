const express = require('express');
const cors = require('cors');
const app = express();
const port = 5000;

app.use(cors());
app.use(express.json());

app.get('/', (req, res) => {
    res.send('My Second Day at Node-JS');
});

const users = [
    {id: 0, name: "Ashraful", email: "ashraful@gmail.com", phone: '122344871312'},
    {id: 1, name: "Akhi", email: "akhi@gmail.com", phone: '45454871312'},
    {id: 2, name: "Naeem", email: "naeem@gmail.com", phone: '5453667312'},
    {id: 3, name: "Shukhy", email: "shukhy@gmail.com", phone: '18774871312'},
    {id: 4, name: "Pakhi", email: "pakhi@gmail.com", phone: '7867844871312'},
    {id: 5, name: "Thaki", email: "thaki@gmail.com", phone: '8768344871312'}
]

app.get('/users', (req, res) => {
    const search = req.query.search;
    // Use query parameter
    if (search) {
        const searchResult = users.filter(user => user.name.toLocaleLowerCase().includes(search));
        res.send(searchResult);
    } else {
        res.send(users);
    }
});

// app.method
app.post('/users', (req, res) => {
    const newUser = req.body;
    newUser.id = users.length;
    users.push(newUser);
    console.log('Hitting the post ', req.body);
    res.send(JSON.stringify(newUser)); // One Way
    res.json(newUser); // Another way

});

// Dynamic Api
app.get('/users/:id', (req, res) => {
    const id = req.params.id;
    const user = users[id];
    res.send(user);
});

app.get('/fruits', (req, res) => {
    res.send(['mango', 'orange', 'apple', 'lychee']);
});

app.get('/fruits/mangoes/langra', (req, res) => {
    res.send('Sweet big mangoes!');
});

app.listen(port, () => {
    console.log('Listening to port ', port);
});