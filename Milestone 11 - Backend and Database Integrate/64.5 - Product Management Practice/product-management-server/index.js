const express = require('express');
const cors = require('cors');
const {ServerApiVersion, MongoClient} = require("mongodb");
const ObjectId = require('mongodb').ObjectId;


const app = express();
const port = 5000;

// Middleware
app.use(cors());
app.use(express.json());

const uri = "mongodb+srv://mydbUser1:7qVmKFpEojiRZDD3@cluster0.dwgia.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";
const client = new MongoClient(uri, {useNewUrlParser: true, useUnifiedTopology: true, serverApi: ServerApiVersion.v1});

async function run() {
    try {
        await client.connect();
        const database = client.db("productManagement");
        const productsCollection = database.collection("products");

        // POST API
        app.post('/products', async (req, res) => {
            const newProduct = req.body;
            const result = await productsCollection.insertOne(newProduct);
            res.json(result);
        });

        // GET API
        app.get('/products', async (req, res) => {
            const cursor = productsCollection.find({});
            const products = await cursor.toArray();
            res.send(products);
        });

        // DELETE API
        app.delete('/products/:id', async (req, res) => {
            const id = req.params.id;
            const query = {_id: ObjectId(id)};
            const result = await productsCollection.deleteOne(query);
            res.json(result);
        });

        // FIND API (Before Update API)
        app.get('/products/:id', async (req, res) => {
            const id = req.params.id;
            const query = {_id: ObjectId(id)};
            const product = await productsCollection.findOne(query);
            res.json(product);
        });

        // UPDATE API (PUT)
        app.put('/products/:id', async (req, res) => {
            const id = req.params.id;
            const updatedProduct = req.body;
            const filter = {_id: ObjectId(id)};
            const options = {upsert: true};
            const updateDoc = {
                $set:{
                    name: updatedProduct.name,
                    price: updatedProduct.price,
                    quantity: updatedProduct.quantity
                }
            };
            const result = await productsCollection.updateOne(filter, updateDoc, options);
            res.json(result);
        });

    } finally {
        // await client.close();
    }
}

run().catch(console.dir);

app.get('/', (req, res) => {
    res.send("Node is Running Ok!")
});

app.listen(port, () => {
    console.log("Listening to port ", port);
});