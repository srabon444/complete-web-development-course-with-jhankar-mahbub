import React, {useEffect, useState} from 'react';
import {useParams} from "react-router-dom";

const UpdateProducts = () => {
    const [product, setProduct] = useState([]);
    const {id} = useParams();

    useEffect(() => {
        const url = `http://localhost:5000/products/${id}`;
        fetch(url)
            .then(res => res.json())
            .then(data => setProduct(data));
    },[]);

    // Handler to make fields editable/writable
    const handleNameChange = e => {
        const updatedName = e.target.value;
        const updatedProduct = {name: updatedName, price: product.price, quantity: product.quantity};
        setProduct(updatedProduct);
    };

    const handlePriceChange = e => {
        const updatedPrice = e.target.value;
        const updatedProduct = {name: product.name, price: updatedPrice, quantity: product.quantity};
        setProduct(updatedProduct);
    };

    const handleQuantityChange = e => {
        const updatedQunatity = e.target.value;
        const updatedProduct = {name: product.name, price: product.price, quantity: updatedQunatity};
        setProduct(updatedProduct);
    };

    // Update Product Handler
    const handleUpdateProduct = e => {
        const url = `http://localhost:5000/products/${id}`;
        fetch(url, {
            method: 'PUT',
            headers: {
                'content-type': 'application/json'
            },
            body: JSON.stringify(product)
        })
            .then(res => res.json())
            .then(data =>{
                if (data.modifiedCount > 0) {
                    alert("Updated Successfully.");
                    setProduct('');
                }
            })
        e.preventDefault();
    };


    return (
        <div>
            <h2>Update: Name: {product.name} => Price: {product.price} => Quantity: {product.quantity}</h2>
            <form onSubmit={handleUpdateProduct}>
                <input type="text" onChange={handleNameChange} value={product.name || ''}/>
                <input type="number" onChange={handlePriceChange} value={product.price || ''}/>
                <input type="number" onChange={handleQuantityChange} value={product.quantity || ''}/>
                <input type="submit" value="Update"/>
            </form>
        </div>
    );
};

export default UpdateProducts;
