const products = [
    "Asus ROG i9 256GB laptop",
    "Asus ROG i5 500GB 8GB Ram laptop",
    "Asus Vivobook laptop",
    "asus Notebook 14inch laptop",
    "Dell Inspiron i7 500GB Laptop",
    "Apple 128GB laptop",
    "Poco X3 128GB Mobile",
    "Lenovo Ryzen 5 16GB RAM Laptop",
    "iphone 13 128GB phone",
    "Samsung Galaxy S20 Ultra Phone"
];

const searching = "laptop";
const output = [];
for (const product of products) {
    if (product.toLowerCase().indexOf(searching.toLowerCase()) !== -1) {
        output.push(product);
    }
}
console.log(output);

// Another way
const output2 = [];
for (const product of products) {
    if (product.toLowerCase().includes(searching.toLowerCase())) {
        output2.push(product);
    }
}
console.log("Another way:", output2);

// starts with check
const startsWith = "Asus";
const output3 = [];
for (const product of products) {
    if (product.toLowerCase().startsWith(startsWith.toLowerCase())) {
        output3.push(product);
    }
}
console.log("starts with check:", output3);