function addNumbers(num1, num2) {
    let result = 0;
    for (const num of arguments) {
        result += num;
    }
    return result;
}

const result = addNumbers(9, 1, 2, 3, 4, 7, 8, 9, 6, 5, 15, 132, 25, 298, 452, 10, 80, 91, 43, 15);
console.log(result);