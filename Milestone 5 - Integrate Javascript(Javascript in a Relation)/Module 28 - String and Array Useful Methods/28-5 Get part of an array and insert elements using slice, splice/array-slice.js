const numbers = [1, 2, 3, 4, 7, 8, 9, 6, 5, 15, 132, 25, 298, 452, 10, 80, 91, 43];
const slicedNumber = numbers.slice(4, 8);
console.log(slicedNumber);

//splice
const numberSpliced = numbers.splice(4, 4);
console.log(numberSpliced);
console.log(numbers);
//Splice and add
const numberSplicedAndAdd = numbers.splice(2, 3, 99, 111, 222, 333, 888, 999);
console.log(numberSplicedAndAdd);
console.log(numbers);