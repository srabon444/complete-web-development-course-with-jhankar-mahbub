var number1 = -56;
var absoluteNumber = Math.abs(number1);
console.log(absoluteNumber);

var number2 = 4.45;
var rounded = Math.round(number2);
console.log(rounded);

var number3 = 4.45;
var ceilinged = Math.ceil(number3);
console.log(ceilinged);

var number4 = 4.45;
var floored = Math.floor(number3);
console.log(floored);

var randomNumber = Math.random();
console.log(randomNumber);

var randomNumber2 = Math.random() * 100;
var result = Math.round(randomNumber2);
console.log(result);