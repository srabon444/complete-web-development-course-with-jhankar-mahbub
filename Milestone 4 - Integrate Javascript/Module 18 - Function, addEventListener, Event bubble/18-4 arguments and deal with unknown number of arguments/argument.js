function addNumbers(number1, number2) {
    let sum = 0;
    for (let i = 0; i < arguments.length; i++) {
        const number = arguments[i];
        sum += number;
    }
    return sum;
}

const result = addNumbers(2, 5, 25, 69, 7);
console.log(result);