import React from 'react';
import './Product.css';

const Product = (props) => {
    // console.log(props.product);
    // const {img, name, price, seller, stock, key} = props.product;
    return (
        <div className='product'>
            <div>
                <img src={props?.product?.img} alt=""/>
            </div>
            <div>
                <h4 className='product-name'>{props?.product?.name}</h4>
                <p><small>by: {props?.product?.seller}</small></p>
                <p>Price: {props?.product?.price}</p>
                <p><small>Only {props?.product?.stock} products left</small></p>
                <button className='btn-regular'>add to cart</button>
            </div>
        </div>

    );
};

export default Product;
