import logo from './logo.svg';
import './App.css';

function App() {
    const products = [
        {name: 'laptop', price: 150000},
        {name: 'mobile', price: 50000},
        {name: 'watch', price: 2000},
        {name: 'shoe', price: 3000},

    ];
    return (
        <div className="App">
            {/*<Products name='apple' price='50'></Products>
            <Products name='orange' price='60'></Products>
            <Products name='pineapple' price='30'></Products>*/}

            {
                products.map((product,index) => <Products key={index} name={product.name} price={product.price}>Gadget</Products>)
            }
        </div>
    );
}

function Products(props) {
    const productStyle = {
        border: '2px solid blue',
    }
    return (
        <div className="product" style={productStyle}>
            <h2>Name: {props.name}</h2>
            <h4>Price: {props.price}</h4>
        </div>
    )
}

export default App;
