import logo from './logo.svg';
import './App.css';
import {useEffect, useState} from "react";

function App() {
    return (
        <div className="App">
            <ExternalUsers></ExternalUsers>
        </div>
    );
}

function ExternalUsers() {
    const [users, setUsers] = useState([]);

    useEffect(() => {
        fetch('https://jsonplaceholder.typicode.com/users')
            .then(res => res.json())
            .then(data => setUsers(data));
    }, []);


    return (
        <div>
            <h1>External Users</h1>
            {
                users.map(user => <User name={user.name} email={user.email} address={user.address.city}>Users</User>)
            }
        </div>
    );
}

function User(props) {
    return (
        <div className='user'>
            <h2>name: {props.name}</h2>
            <p>email: {props.email}</p>
            <p>address: {props.address}</p>
        </div>
    );
}

export default App;
