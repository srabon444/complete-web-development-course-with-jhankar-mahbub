import logo from './logo.svg';
import './App.css';
import {useState} from "react";

function App() {
    return (
        <div className="App">
            <Counter></Counter>
        </div>
    );
}

function Counter() {
    const [count, setCount] = useState(0);
    // Handles Increase
    const handleIncrease = () => {
        const newCount = count + 1;
        setCount(newCount);
    }

    // Handles Decrease (Compact code)
    let handleDecrease;
    if (count > 0) {
        handleDecrease = () => setCount(count - 1);
    }
    return (
        <div>
            <h1>Count: {count}</h1>
            <button onClick={handleIncrease}>Increase</button>
            <button onClick={handleDecrease}>Decrease</button>
        </div>
    );
}

export default App;
