import logo from './logo.svg';
import './App.css';
import {useState} from "react";

function App() {
    return (
        <div className="App">
            <MyComponent brand='Apple' price='90000'>Apple</MyComponent>
            <MyComponent brand='Microsoft' price='10000'>Microsoft</MyComponent>
            <MyComponent brand='Google' price='0'>Google</MyComponent>
            <MyComponent>N/A</MyComponent>
        </div>
    );
}

function MyComponent(props) {
    const [points, setPoints] = useState(1);
    const myStyle = {
        backgroundColor: 'rgb(229, 227, 201)',
        color: 'rgb(54, 39, 6)',
        border: '2px solid green',
        borderRadius: '10px',
        margin: '10px',
        padding: '5px'
    };

    const handleAddPoints = () => {
        const newPoints = points * 2;
        setPoints(newPoints);
    }

    return (
        <div style={myStyle}>
            <h1>Inside Component. Brand: {props.brand}</h1>
            <h4>Asking price: {props.price}, Points: {points}</h4>
            <button onClick={handleAddPoints}>Add Points</button>
            <p style={{fontWeight: 'bold'}}>Paragraph from inside Component</p>
        </div>
    )
}

export default App;
