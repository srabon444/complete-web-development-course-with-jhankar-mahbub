import './App.css';
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import About from "./About/About";
import ContactUs from "./ContactUs/ContactUs";
import Header from "./Header/Header";
import Footer from "./Footer/Footer";
import Home from "./Home/Home";
import MoreItems from "./MoreItems/MoreItems";


function App() {
    return (
        <div className="App">
            <Router>
                <Header/>
                <Switch>
                    <Route path="/home">
                        <Home/>
                    </Route>
                    <Route path="/about">
                        <About/>
                    </Route>
                    <Route path="/contact">
                        <ContactUs/>
                    </Route>
                    <Route path="/more">
                        <MoreItems/>
                    </Route>
                    {/*<Route path="*">
                        <NotFound/>
                    </Route>*/}
                </Switch>
                <Footer/>
            </Router>
        </div>
    );
}

export default App;
