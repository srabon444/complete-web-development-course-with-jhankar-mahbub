import React from 'react';
import TopItems from "../TopItems/TopItems";
import MiddleItems from "../MiddleItems/MiddleItems";
import MoreItems from "../MoreItems/MoreItems";

const Home = () => {
    return (
        <div>
            <TopItems/>
            <MiddleItems/>
            <MoreItems/>
        </div>
    );
};

export default Home;
