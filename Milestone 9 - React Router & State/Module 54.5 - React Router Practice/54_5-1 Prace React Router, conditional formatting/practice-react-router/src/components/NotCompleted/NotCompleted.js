import React, {useEffect, useState} from 'react';
import useTodos from "../../hooks/useTodos";
import Todos from "../Todos/Todos";

const NotCompleted = () => {
    const [todos, setTodos] = useTodos();
    const [allNotComplete, setAllNotComplete] = useState([]);

    useEffect(() => {
        const notCompleteList = todos.filter(todo => todo.completed === false);
        setAllNotComplete(notCompleteList);
    }, [todos]);

    return (
        <div>
            <h2>This is Not Completed List</h2>
            <h3>{allNotComplete.length}, tasks not completed.</h3>
            {
                allNotComplete.length > 0 &&
                allNotComplete.map(todo => <Todos
                    key={todo.id}
                    todo={todo}
                ></Todos>)
            }
        </div>
    );
};

export default NotCompleted;
