import React from 'react';

const Todos = (props) => {
    const {title, completed} = props.todo;

    return (
        <div>
            <h4>Title: {title}</h4>
            <p>Completed: {completed ? "true" : "false"}</p>
        </div>
    );
};

export default Todos;
