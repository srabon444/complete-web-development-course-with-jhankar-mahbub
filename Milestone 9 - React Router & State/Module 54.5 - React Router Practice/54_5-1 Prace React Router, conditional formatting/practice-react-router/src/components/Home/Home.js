import React from 'react';
import useTodos from "../../hooks/useTodos";
import Todos from "../Todos/Todos";

const Home = () => {
    const [todos, setTodos] = useTodos();

    return (
        <div>
            <h2>This is Home</h2>
            <br/>
            <button>Add Todo</button>

            {
                todos.length>0 &&
                todos.map(todo => <Todos
                    key={todo.id}
                    todo={todo}
                >
                </Todos>)
            }
        </div>
    );
};

export default Home;
