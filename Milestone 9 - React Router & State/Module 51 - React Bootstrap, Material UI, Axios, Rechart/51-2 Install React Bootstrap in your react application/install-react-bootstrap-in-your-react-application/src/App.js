import logo from './logo.svg';
import './App.css';
import {Button, Spinner} from "react-bootstrap";
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
    return (
        <div className="App">
            <Button variant='primary'>Click Me</Button>
            <Spinner animation="border" variant="success"/>
        </div>
    );
}

export default App;
