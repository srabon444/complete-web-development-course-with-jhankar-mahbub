import './App.css';
import {Button, Rating} from "@mui/material";
import * as React from 'react';


function App() {
    return (
        <div className="App">
            <Button variant='contained'>Click Me</Button>
            <Rating
                name="simple-controlled"
                value={4}
                readOnly
            />
        </div>
    );
}

export default App;
