import './App.css';
import React from 'react';
import MyLineChart from "./components/MyLineChart/MyLineChart";

function App() {

    return (
        <div className="App">
            <MyLineChart></MyLineChart>
        </div>
    );
}

export default App;
