import logo from './logo.svg';
import './App.css';
import Card from "./components/Card/Card";

function App() {
    const items = [
        {
            name: 'First Item',
            description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda autem commodi consectetur, cupiditate, dolore iusto laudantium nemo nulla, quis quisquam recusandae rem ? Aut dignissimos exercitationem obcaecati quia quidem soluta veniam.',
            image: 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/sunset-quotes-21-1586531574.jpg'
        },
        {
            name: 'Second Item',
            description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda autem commodi consectetur, cupiditate, dolore iusto laudantium nemo nulla, quis quisquam recusandae rem ? Aut dignissimos exercitationem obcaecati quia quidem soluta veniam.',
            image: 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/sunset-quotes-21-1586531574.jpg'
        },
        {
            name: 'Third Item',
            description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda autem commodi consectetur, cupiditate, dolore iusto laudantium nemo nulla, quis quisquam recusandae rem ? Aut dignissimos exercitationem obcaecati quia quidem soluta veniam.',
            image: 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/sunset-quotes-21-1586531574.jpg'
        },
        {
            name: 'Fourth Item',
            description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda autem commodi consectetur, cupiditate, dolore iusto laudantium nemo nulla, quis quisquam recusandae rem ? Aut dignissimos exercitationem obcaecati quia quidem soluta veniam.',
            image: 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/sunset-quotes-21-1586531574.jpg'
        },
        {
            name: 'Fifth Item',
            description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda autem commodi consectetur, cupiditate, dolore iusto laudantium nemo nulla, quis quisquam recusandae rem ? Aut dignissimos exercitationem obcaecati quia quidem soluta veniam.',
            image: 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/sunset-quotes-21-1586531574.jpg'
        },
        {
            name: 'Sixth Item',
            description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda autem commodi consectetur, cupiditate, dolore iusto laudantium nemo nulla, quis quisquam recusandae rem ? Aut dignissimos exercitationem obcaecati quia quidem soluta veniam.',
            image: 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/sunset-quotes-21-1586531574.jpg'
        },

    ]
    return (
        <div className="row row-cols-1 row-cols-md-4 g-4">
            {
                items.map(item => <Card
                item={item}
                ></Card>)
            }
        </div>
    );
}

export default App;
