import React from 'react';
import Special from "../Special/Special";

const Myself = (props) => {
    const {house} = props;
    return (
        <div>
            <h2>Myself</h2>
            <small>House: {house}</small>
            <Special/>
        </div>
    );
};

export default Myself;
