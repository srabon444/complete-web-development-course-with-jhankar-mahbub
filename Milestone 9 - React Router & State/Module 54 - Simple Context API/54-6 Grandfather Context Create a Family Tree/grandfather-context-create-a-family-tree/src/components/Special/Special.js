import React, {useContext} from 'react';
import {RingContext} from "../../App";

const Special = () => {
    const [ornaments, house] = useContext(RingContext);

    return (
        <div>
            <h2>Special</h2>
            <small>Ornaments: {ornaments}</small>
            <br/>
            <small>Context House: {house}</small>
        </div>
    );
};

export default Special;
