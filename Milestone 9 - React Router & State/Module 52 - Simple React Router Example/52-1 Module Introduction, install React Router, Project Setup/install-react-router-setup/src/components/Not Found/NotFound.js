import React from 'react';

const NotFound = () => {
    return (
        <div>
            <h3>Page Not Found</h3>
            <p>Something went wrong.</p>
        </div>
    );
};

export default NotFound;
