import React from 'react';

const About = () => {
    return (
        <div>
            <h2>This is About page</h2>
            <p>Contact us to learn more</p>
        </div>
    );
};

export default About;
