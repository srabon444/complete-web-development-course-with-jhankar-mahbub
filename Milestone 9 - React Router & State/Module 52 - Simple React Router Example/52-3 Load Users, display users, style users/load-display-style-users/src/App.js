import logo from './logo.svg';
import './App.css';
import Friends from "./components/Friends/Friends";
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import About from "./components/About/About";
import Home from "./components/Home/Home";
import NotFound from "./components/NotFound/NotFound";
import Header from "./components/Header/Header";
import FriendDetail from "./components/FriendDetail/FriendDetail";
import Culture from "./components/Culture/Culture";
import PostDetails from "./components/PostDetails/PostDetails";

function App() {
    return (
        <div className="App">
            <Router>
                <Header/>
                <Switch>
                    <Route exact path='/'>
                        <Home/>
                    </Route>
                    <Route path='/home'>
                        <Home/>
                    </Route>
                    <Route path='/friends'>
                        <Friends/>
                    </Route>
                    <Route path='/friend/:friendId'>
                        <FriendDetail/>
                    </Route>
                    <Route path='/post/:postId'>
                        <PostDetails/>
                    </Route>
                    <Route exact path='/about'>
                        <About/>
                    </Route>
                    <Route exact path='/about/culture'>
                        <Culture/>
                    </Route>
                    <Route path='*'>
                        <NotFound/>
                    </Route>
                </Switch>
            </Router>
        </div>
    );
}

export default App;
