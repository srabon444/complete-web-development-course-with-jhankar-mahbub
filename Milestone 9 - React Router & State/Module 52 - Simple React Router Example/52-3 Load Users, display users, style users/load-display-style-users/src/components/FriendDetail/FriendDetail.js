import React, {useEffect, useState} from 'react';
import {useHistory, useParams} from "react-router-dom";

const FriendDetail = () => {
    const {friendId} = useParams();
    const [friend, setFriend] = useState({});
    const history = useHistory();

    useEffect(() => {
        const url = `https://jsonplaceholder.typicode.com/users/${friendId}`;
        fetch(url)
            .then(res => res.json())
            .then(data => setFriend(data));
    }, []);

    const handleSeeAllFriendsClick = () => {
        history.push('/friends');
    };

    return (
        <div>
            <h2>Friend detail of: {friendId}</h2>
            <h1>{friend.name}</h1>
            <h2>{friend.phone}</h2>
            <p><small>{friend.website}</small></p>
            <p>Works at: {friend?.company?.name}</p>
            <button onClick={handleSeeAllFriendsClick}>See All Friends</button>
        </div>
    );
};

export default FriendDetail;
