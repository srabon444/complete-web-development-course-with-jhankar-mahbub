import React from 'react';
import Posts from "../Posts/Posts";

const Home = () => {
    return (
        <div>
            <h3>This is Home</h3>
            <Posts/>
        </div>
    );
};

export default Home;
