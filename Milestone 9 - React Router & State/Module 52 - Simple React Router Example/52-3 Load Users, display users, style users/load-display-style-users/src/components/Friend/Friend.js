import React from 'react';
import {Link, useHistory} from "react-router-dom";


const Friend = (props) => {
    const {id, name, email, phone, website, address} = props.friend;
    const history = useHistory();
    const friendStyle ={
        border: '3px solid goldenrod',
        padding: '10px',
        borderRadius: '10px',
        margin: '6px'
    };
    const url = `/friend/${id}`;
    const handleFriendClick = () => {
        history.push(`/friend/${id}`);
    };
    return (
        <div style={friendStyle}>
            <h2>I am {name} {id}</h2>
            <p>Email: {email}</p>
            <p><small>Phone: {phone}</small></p>
            <p><small>Website: {website}</small></p>
            <p>Address: {address.city}</p>
            <Link to={url}>Visit Me</Link>
            <br/>
            {/*Using Button to navigate dynamic/fixed route*/}
            {/*Option - 1*/}
            <Link to={url}>
                <button>Visit Me</button>
            </Link>
            <br/>
            {/*Option - 2*/}
            <button onClick={handleFriendClick}>Visit Me 2</button>

        </div>
    );
};

export default Friend;
