for (let i = 0; i < 10; i++) {
    console.log(i);
}

console.log("lorem text");
function add(num1, num2) {
    return num1 + num2;
}

console.log(add(2, 3));
console.log("Hello");
console.log("Javascript");
console.log(add(4, 5));

function getLargestNumber(numbers) {
    var largest = numbers[0];
    for (let i = 0; i < numbers.length; i++) {
        if (numbers[i] > largest) {
            largest = numbers[i];
        }
    }
    return largest;
}

var output = getLargestNumber([2, 5, 9, 8, 7, 3, 0]);
console.log('Largest: ', output);