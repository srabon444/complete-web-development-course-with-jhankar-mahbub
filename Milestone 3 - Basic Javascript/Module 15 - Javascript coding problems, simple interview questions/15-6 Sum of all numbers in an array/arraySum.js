
function arraySum(array) {
    let sum = 0;
    for (let i = 0; i < array.length; i++) {
        sum += array[i];
    }
    return sum;
}

let numbers = [2, 5, 25, 65, 74, 15, 23, 36, 98, 7, 9, 68, 321, 47, 58, 21];
console.log("Sum = " + arraySum(numbers));
console.log("Sum = " + arraySum([1, 5, 8, 9]));
