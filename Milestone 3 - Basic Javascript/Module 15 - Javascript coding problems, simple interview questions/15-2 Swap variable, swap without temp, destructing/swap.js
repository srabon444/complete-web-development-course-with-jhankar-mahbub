let a = 4;
let b = 8;
let temp;
console.log("Before swap a = " + a + ", b = " + b);
temp = a;
a = b;
b = temp;
console.log("After swap a = " + a + ", b = " + b);

/*2nd way without temp variable*/
console.log("2nd way without temp variable");
let x = 4;
let y = 8;
console.log("Before swap x = " + x + ", y = " + y);
x = x + y;
y = x - y;
x = x - y;
console.log("After swap x = " + x + ", y = " + y);
/*Third way*/
console.log("3rd way");
let p = 5;
let q = 7;
console.log("Before swap p = " + p + ", q = " + q);
[p, q] = [q, p];
console.log("After swap p = " + p + ", q = " + q);