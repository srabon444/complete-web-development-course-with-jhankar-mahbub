num = 6;
switch (num) {
    case 1000:
        console.log('I am 1000');
        break;
    case 10:
        console.log('I am 10');
        break;
    case 100:
        console.log('I am 100');
        break;
    case 23:
        console.log('I am 23');
        break;
    case 5:
        console.log('I am 5');
        break;
    default:
        console.log('Number not found');
        break;

}