var student1 = {id:122, phone:123456789, name: "Ashraful"}
var student2 = {id:143, phone:123453489, name: "Akhi"}

console.log(student1);
console.log(student2);
var phone = student1.phone;
console.log(phone);

var id = student1["id"];
console.log(id);

var name = "name";
var student1Name = student1[name];
console.log(student1Name);

//update phone number
student2.phone = 999999;
console.log(student2.phone);
console.log(student2);

//New property add
student2.breakfast = "Cereal";
console.log(student2);
