function doubleIt(num) {
    var result = num * 2;
    return result;
}

var first = doubleIt(2);
var second = doubleIt(8);
console.log(first, second);
var total = first + second;
console.log(total);

function add(num1, num2) {
    var result = num1 + num2;
    return result;
}

var sum = add(2, 3);
console.log(sum);
