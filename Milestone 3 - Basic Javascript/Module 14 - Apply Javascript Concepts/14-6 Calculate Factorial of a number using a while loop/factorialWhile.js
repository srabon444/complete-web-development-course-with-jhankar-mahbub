function factorialWhile(num) {
    let i = 1;
    let factorial = 1;
    while (i <= num) {
        factorial *= i;
        i++;
    }
    return factorial;
}
console.log(factorialWhile(10));