function fibonacciSeriesRecursive(n) {
    if (n == 0) {
        return [0];
    }
    else if (n == 1) {
        return [0, 1];
    } else {
        let fib = fibonacciSeriesRecursive(n - 1);
        let nextElement = fib[n - 1] + fib[n - 2];
        fib.push(nextElement);
        return fib;
    }
}

console.log(fibonacciSeriesRecursive(10));