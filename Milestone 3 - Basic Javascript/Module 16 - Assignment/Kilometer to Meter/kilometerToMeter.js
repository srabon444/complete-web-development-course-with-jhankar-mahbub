function kilometerToMeter(kilometer) {
    let meter;
    if (kilometer <= 0) {
        return "Input must be greater or, equal to 1.";
    } else {
        meter = kilometer * 1000;
    }
    return meter;
}

console.log(kilometerToMeter(25));