function add(num1, num2) {
    return num1 + num2;
}

console.log(add(1, 2));

// function expression
const add2 = function add2(num1, num2) {
    return num1 + num2;
};

console.log(add(2, 2));

// function expression (anonymous)
const add3 = function (num1, num2) {
    return num1 + num2;
};

console.log(add(3, 2));

// arrow function
const add4 = (num1, num2) => num1 + num2;
console.log(add(4, 2));