const add = (num1, num2) => num1 + num2;
console.log(add(2, 2));

const multiply = (num1, num2, num3) => num1 * num2 * num3;
console.log(multiply(2, 3, 4));

const fiveTimes = num => num * 5;
console.log(fiveTimes(4));

const getName = () => "Ashraful Islam";
console.log(getName());

const doMath = (x,y) => {
    const sum = x + y;
    const sub = x - y;
    const multiply = x * y;
    return multiply;
}

const multiplication = doMath(2, 3);
console.log(multiplication);