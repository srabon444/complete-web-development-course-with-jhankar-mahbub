const student = {
    id: 102,
    name: 'Ashraful Islam',
    major: "Computer Science",
    isRich: false,
    money: 5000,
    subjects: ['Algorithm', 'Math-101', 'Economics'],
    bestFriend: {
        name: 'Akhi',
        major: 'Computer Science'
    },
    takeExam: function () {
        console.log(this.name, ' taking exam.');
    },
    expenditure: function (expense) {
        this.money = this.money - expense;
        return this.money;
    },
};

student.takeExam();
const remaining = student.expenditure(900);
console.log(remaining);