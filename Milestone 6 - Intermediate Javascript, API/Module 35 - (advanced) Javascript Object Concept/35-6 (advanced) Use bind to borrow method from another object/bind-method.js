const ashraful = {
    id: 102,
    money: 5000,
    name: "Ashraful Islam",
    expenditure: function (expense) {
        this.money = this.money - expense;
        console.log(this);
        return this.money;
    },
};

const akhi = {
    id: 103,
    money: 7000,
    name: "Akhi",
    expenditure: function (expense) {
        this.money = this.money - expense;
        console.log(this);
        return this.money;
    },
};

ashraful.expenditure(100);
const akhiExpense = akhi.expenditure.bind(akhi);
akhiExpense(400);