// 1. Using object literal
const person = {name: 'John reese', job: 'Secret Agent'}

// 2. Constructor
const student = new Object();
console.log(student);

// 3. Inheritance
/*
const human = Object.create(null);
console.log(human);*/

const human = Object.create(person);
console.log(human.job);

// 4. Object create with class
class People {
    constructor(name, age) {
        this.name = name;
        this.age = age;
    }
}

const people = new People('Human', 35);
console.log(people);

// 5. function object create
function SuperHero(name) {
    this.name = name;
}

const man = new SuperHero('IronMan');
console.log(man);