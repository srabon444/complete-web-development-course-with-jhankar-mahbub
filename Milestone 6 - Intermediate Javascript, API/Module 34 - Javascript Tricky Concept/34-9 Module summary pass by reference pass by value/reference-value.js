function triple(x, y, z) {
    x = 111;
    y = 2222;
    z = 888;
}

const num1 = 3;
const num2 = 4;
const num3 = 9;
triple(num1, num2, num3);
console.log(num1, num2, num3);

function objRef(x) {
    x.age = 111;
}

const myObject = {name: "Ashraful", age: 21}
objRef(myObject);
console.log(myObject);