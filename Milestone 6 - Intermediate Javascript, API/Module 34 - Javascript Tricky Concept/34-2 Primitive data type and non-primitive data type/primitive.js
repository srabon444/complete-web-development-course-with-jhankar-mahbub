let a = 5;
let b = a;
console.log(a, b);
a = 6;
console.log(b);

const x = {job: 'Web - Developer'}
console.log(x);
const y = x;
x.job = 'Front-End Developer';
console.log(x, y);
y.job = 'Back-End Developer';
console.log(x, y);