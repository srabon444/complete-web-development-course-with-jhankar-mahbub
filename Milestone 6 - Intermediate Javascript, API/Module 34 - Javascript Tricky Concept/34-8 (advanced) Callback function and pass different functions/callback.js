function welcomeMessage(name, greetHandler) {
    console.log(greetHandler);
    greetHandler(name);
}

// const name = ['Tom Holland','Tom Cruise'];
// const myObject = {name:"Ashraful", age: 20}
// welcomeMessage(myObject);
function greetings(name) {
    console.log("Good Morning ", name);
}

function greetingsAfternoon(name) {
    console.log("Good Afternoon ", name);
}
welcomeMessage('Ashraful Islam', greetings);
welcomeMessage('Akhi', greetingsAfternoon);

