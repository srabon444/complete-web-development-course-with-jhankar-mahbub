let x = false;
if (x) {
    console.log("x is true");
} else {
    console.log("x is false");
}

x = true;
if (x) {
    console.log("x is true");
} else {
    console.log("x is false");
}

x = 4;
if (x) {
    console.log("x is true");
} else {
    console.log("x is false");
}

x = -7;
if (x) {
    console.log("x is true");
} else {
    console.log("x is false");
}

x = 0;
if (x) {
    console.log("x is true");
} else {
    console.log("x is false");
}

x = "Ashraful Islam";
if (x) {
    console.log("x is true");
} else {
    console.log("x is false");
}

x = '';
if (x) {
    console.log("x is true");
} else {
    console.log("x is false");
}

let y;
console.log(y);
if (y) {
    console.log("y is true");
} else {
    console.log("y is false");
}


x = null;
if (x) {
    console.log("x is true");
} else {
    console.log("x is false");
}

y = parseInt("Hello");
console.log(y);
if (y) {
    console.log("y is true");
} else {
    console.log("y is false");
}

y =[];
console.log(y);
if (y) {
    console.log("y is true");
} else {
    console.log("y is false");
}

y = {};
// Empty object
console.log(y);
if (y) {
    console.log("y is true");
} else {
    console.log("y is false");
}