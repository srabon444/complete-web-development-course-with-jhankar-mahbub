let first;
console.log(first);

function second(x, y) {
    const sum = x + y;
}

const result = second(2, 3);
console.log(result);

function add(a, b) {
    const sum = a + b;
    if (a < b) {
        return;
    }
    const fun = a * b;
    return sum;
}

const fourth = add(2, 1);
console.log(fourth);

function double(a,b){
    const result = a * b;
    console.log(b);
    return result;
}

double(4);

const fifth = { name:"john", age:18, location: "NYC"}
console.log(fifth.phoneNumber);

const sixth = [3, 6, 7, 9];
console.log(sixth[90]);

delete sixth[2];
console.log(sixth[2]);

const seventh = undefined;
console.log(seventh);


