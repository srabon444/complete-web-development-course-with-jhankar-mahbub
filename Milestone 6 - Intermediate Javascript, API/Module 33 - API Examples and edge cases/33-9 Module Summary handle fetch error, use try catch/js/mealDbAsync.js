const searchFood = async () => {
    const searchField = document.getElementById('search-field');
    const searchText = searchField.value;
    // console.log(searchText);
    //Clear Data
    searchField.value = '';
    if (searchText === '') {
        const div = document.createElement('div');
        div.innerHTML = `
        <h4 class="text-center text-danger">Write Something to Search</h4>
        `;
        searchField.appendChild(div);
    } else {
        // Load Data
        const url = `https://www.themealdb.com/api/json/v1/1/search.php?s=${searchText}`;
        // console.log(url);
        try {
            const res = await fetch(url);
            const data = await res.json();
            displaySearchResult(data.meals);
        } catch (error){
            console.log(error);
        }
        // fetch(url)
        //     .then(res => res.json())
        //     .then(data => displaySearchResult(data.meals));
    }
}

const displaySearchResult = meals => {
    const searchResult = document.getElementById('search-result');
    //Clears previous results when again searched
    searchResult.textContent = '';
    //Displays message when nothing found in search
    if (meals.length >= 1) {
        meals.forEach(meal => {
            const div = document.createElement('div');
            div.classList.add('col');
            div.innerHTML = `
        <div onclick="loadMealDetail(${meal.idMeal})" class="card h-100">
                <img src="${meal.strMealThumb}" class="card-img-top" alt="...">
                <div class="card-body">
                <h5 class="card-title">${meal.strMeal}</h5>
                <p class="card-text">${meal.strInstructions.slice(0, 200)}</p>
                </div>
        </div>
    `;
            searchResult.appendChild(div);
        });
    } else {
        const div = document.createElement('div');
        div.innerHTML = `
        <h4 class="text-center text-danger">Nothing Found</h4>
        `;
        searchResult.appendChild(div);
    }
}

const loadMealDetail = async mealId => {
    const url = `https://www.themealdb.com/api/json/v1/1/lookup.php?i=${mealId}`;
    const res = await fetch(url);
    const data = await res.json();
    displayMealDetail(data.meals[0]);
    // fetch(url)
    //     .then(res => res.json())
    //     .then(data => displayMealDetail(data.meals[0]));
}

const displayMealDetail = meal => {
    const mealDetails = document.getElementById('meal-details');
    mealDetails.textContent = '';
    const div = document.createElement('div');
    div.classList.add('card');
    div.innerHTML = `
    <img src="${meal.strMealThumb}" class="card-img-top" alt="...">
        <div class="card-body">
            <h5 class="card-title">${meal.strMeal}</h5>
            <p class="card-text">${meal.strInstructions.slice(0, 150)}</p>
            <a href="${meal.strYoutube}" class="btn btn-primary">Youtube Video</a>
        </div>
    `;
    mealDetails.appendChild(div);
}