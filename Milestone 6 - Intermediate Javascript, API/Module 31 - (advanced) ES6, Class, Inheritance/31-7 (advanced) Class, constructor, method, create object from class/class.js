class Support {
    name;
    designation = "Support Web Dev";
    address = "Bangladesh";

    constructor(name, address) {
        this.name = name;
        this.address = address;
    }

    startSession() {
        console.log(this.name, "Start a support session");
    }
}

const ashraful = new Support("Ashraful Islam", "Chittagong");
const akhi = new Support("Akhi", "Dhaka");
console.log(ashraful);
console.log(akhi);
ashraful.startSession();