// declare variable based on the name of an object property
const myObject = {x: 2, y: 5, z: 9, a: 45, b: 85};
const {x, b} = myObject;
console.log(b);

// destructuring array
const [p, q] = [45, 36, 76, 90];
console.log(p, q);

// chaining
const company = {
    name: "Galaxy Tech",
    ceo: {id: 1, name: "Ashraful Islam", responsibility: "Maintain Company"},
    web: {
        work: "Website Development",
        employee: 17,
        framework: "React",
        tech: {first: "html", second: "css", third: "js"}
    },
    Income: 100000
}

console.log(company.web.tech.first);
console.log(company?.web?.backend?.third);
