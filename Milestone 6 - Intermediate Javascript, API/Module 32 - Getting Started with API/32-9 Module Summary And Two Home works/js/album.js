function loadAlbum() {
    fetch('https://jsonplaceholder.typicode.com/albums')
        .then(response => response.json())
        .then(albums => displayAlbums(albums));
}

loadAlbum();

function displayAlbums(albums) {
    const albumContainer = document.getElementById('albums');
    for (const album of albums) {
        const p = document.createElement('p');
        p.innerText = album.title;
        albumContainer.appendChild(p);
    }

}

function loadPhotos() {
    fetch('https://jsonplaceholder.typicode.com/photos')
        .then(response => response.json())
        .then(photos => displayPhotos(photos));
}

loadPhotos();
function displayPhotos(photos) {
    const photosContainer = document.getElementById('photos');
    for (const photo of photos) {
        const img = document.createElement('img');
        img.src = photo.url;
        document.body.appendChild(img);
    }
}