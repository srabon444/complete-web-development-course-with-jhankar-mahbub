// JSON
//Javascript Object Notation
const user = {id:11, name: "Ashraful Islam", job: "Software Engineer"}
const stringified = JSON.stringify(user);
console.log(user);
console.log(stringified);

const shop = {
    name: "Dhaka Vulcanizing",
    owner: {
        name: "MD. Hanif Mia",
        profession: "Mechanic"
    },
    products: ["Tire", "Tube", "Rim"],
    profit: 15000,
    isExpensive: false
};
const shopStrigified = JSON.stringify(shop);
// console.log(shop);
console.log(shopStrigified);
const converted = JSON.parse(shopStrigified);
console.log(converted);
