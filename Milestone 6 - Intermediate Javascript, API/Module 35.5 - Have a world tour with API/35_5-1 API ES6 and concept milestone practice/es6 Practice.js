/*
১. কখন const আর কখন let দিয়ে ভেরিয়েবল ডিক্লেয়ার করতে হয় সেটা তোমাকে জানতেই হবে। তুমি নিজে নিজে একটা const দিয়ে আরেকটা let দিয়ে ভেরিয়েবল ডিক্লেয়ার করে ফেলো। */
const name = "Ashraful Islam";
let number = 45;
/*console.log(name);
console.log(number);*/

/*
২. টেম্পলেট স্ট্রিং দিয়ে একটা স্ট্রিং তৈরি করো। সেটার মধ্যে উপরে ডিক্লেয়ার করা ভেরিয়েবল এর মান ডাইনামিক ভাবে বসাও। একইভাবে উপরে একটা অবজেক্ট ডিক্লেয়ার করো। এবং ডাইনামিকভাবে উপরের অবজেক্ট এর দুইটা প্রপার্টি এর মান তোমার টেমপ্লেট স্ট্রিং এর মধ্যে বসাও।  */
const products = {
    laptopBrand: 'Asus',
    Model: 'GL552V',
    color: 'Black',
    price: 75000,
    mobile: {
        brand: 'Xiaomi',
        color: 'Steel Blue',
        price: 21000
    }
};

const myProducts = `My Laptop brand is ${products.laptopBrand} and the price is ${products.price}. My mobile brand is ${products.mobile.brand} and it's price is ${products.mobile.price}`;
// console.log(myProducts);

/*
৩.১ একটা প্যারামিটার ওয়ালা arrow ফাংশন ডিক্লেয়ার করো। এবং সেই ফাংশনের কাজ হবে তুমি কোন ইনপুট দিলে সেই ইনপুট সংখ্যাকে ৫ দিয়ে ভাগ করে আউটপুট দিবে। */
const division = number => number / 5;
const output = division(number);
// console.log(output);

/*
৩.২ তুমি দুইটা প্যারামিটার ওয়ালা একটা অ্যারো ফাংশন লিখবে। সেই ফাংশনের ভিতরে কাজ হবে। প্রত্যেকটা ইনপুট প্যারামিটার এর সাথে ২ যোগ করবে তারপর যোগফল দুইটা গুণ করবে। ক্যামনে করবে সেটা চিন্তা করে বের করার চেষ্টা করো */
const result = (number1, number2) => (number1 + 2) * (number2 + 2);
// console.log(result(2, 3));

/*
৩.৩ এইবার তিনটা প্যারামিটার ওয়ালা একটা অ্যারো ফাংশন ডিক্লেয়ার করো। যেই ফাংশনের কাজ হবে তিনটা প্যারামিটার নিয়ে সেই তিনটা প্যারামিটারকে গুণ করে সেই রেজাল্ট রিটার্ন করবে। */
const multiplication = (number1, number2, number3) => {
    return (number1 * number2 * number3);
};
// console.log(multiplication(2, 3, 4));

/*
৩.৪ এইবার দুইটা প্যারামিটার ওয়ালা একটা অ্যারো ফাংশন নিবে। ওই arrow ফাংশনটা হবে অনেকগুলা লাইনের। সেখানে প্রত্যেকটা ইনপুট প্যারামিটার এর সাথে ২ যোগ করবে তারপর যোগফল দুইটা গুণ করবে। ক্যামনে করবে সেটা চিন্তা করে বের করার চেষ্টা করো।
 */
const multSum = (number1, number2) => {
    return ((number1 + 2) * (number2 + 2));
};
// console.log(multSum(2, 3));

/*
৪.[হোম ওয়ার্ক] একটু গুগলে সার্চ দাও: javascript function declaration vs arrow function তারপর কয়েকটা আর্টিকেল পড়ে বুঝার চেষ্টা করো। */
/* ৪.৫.[এক্সট্রা আরেকটা হোম ওয়ার্ক। এইটা ভিডিওতে বলা নাই]: জাভাস্ক্রিপ্ট এর var, let, const এর মধ্যে প্রার্থক্য কি?  */

/*
৫.অনেকগুলা সংখ্যার একটা array হবে। তারপর তোমার কাজ হবে array এর উপরে map ইউজ করে। প্রত্যেকটা উপাদানকে ৫ দিয়ে গুন্ করে গুনফল আরেকটা array হিসেবে রাখবে। পুরা কাজটা এক লাইনে হবে। */
const number2 = [15, 2, 78, 96, 31, 28, 45, 95, 21, 9, 8, 5, 3];
const multiplicationMap = number2.map(x => x * 5);
// console.log(multiplicationMap);

/*
৬.[চ্যালেঞ্জিং। গুগলে সার্চ দিয়ে বের করো] অনেকগুলা সংখ্যার একটা array থেকে শুধু বিজোড় সংখ্যা বের করে নিয়ে আসার জন্য filter ইউজ করো */
const number3 = [15, 2, 78, 96, 31, 28, 45, 95, 21, 9, 8, 5, 3];
const oddFilter = number3.filter(x => x % 2);
// console.log(oddFilter);

/* ৭.একটা array এর মধ্যে অনেকগুলা অবজেক্ট আছে। সেখানে যেই অবজেক্ট এর price আছে ৫০০০ টেক্কা সেই অবজেক্টকে find দিয়ে বের করো। */
const products2 = [
    {name: 'TV', price: 2000},
    {name: 'Fridge', price: 1800},
    {name: 'Laptop', price: 1900},
    {name: 'Gaming-Rig', price: 5000}
];
const expensive = products2.find(product => product.price === 5000);
// console.log(expensive);

/* ৭.৫[এক্সট্রা] জাভাস্ক্রিপ্ট এ array এর map, forEach, filter, find কোনটা দিয়ে কি হয়। সেটার একটা সামারি লিখে ফেলো।  */

/* ৮.সিম্পল একটা জাভাস্ক্রিপ্ট অবজেক্ট এর কোন একটা প্রোপার্টিকে ভেরিয়েবল হিসেবে ডিক্লেয়ার করার জন্য destructuring ইউজ করো। */

const laptop = {
    brand: 'Dell',
    screen: 'LED 1080p',
    keyboard: 'Backlight',
    ram: '16GB',
    processor: 'AMD Ryzen',
    storage: 512
}

const {brand, processor} = laptop;
// console.log(brand, processor);

/* ৯.[চ্যালেঞ্জিং] array এর destructuring করবে আর সেটা করার জন্য তুমি এরে এর থার্ড পজিশন এর উপাদান কে destructuring করে 'three' নামক একটা ভেরিয়েবল এ রাখবে। */
const arrayName = ['Ashraful', 'Akhi', 'Naeem', 'Shukhy'];
const [, , selectedName] = arrayName;
// console.log(selectedName);

/* ১০.তিনটা প্যারামিটার ওয়ালা একটা ফাংশন লিখবে। যেই ফাংশনের কাজ হবে তিনটা প্যারামিটার নিয়ে সেই তিনটা প্যারামিটার এর যোগ করে যোগফল রিটার্ন করবে। আর থার্ড প্যারামিটার এর একটা ডিফল্ট ভ্যালু থাকবে। সেটা হবে ৭।  */
const sum = (number1, number2, number3 = 7) => {
    return number1 + number2 + number3;
}
const total = sum(2, 3, 4);
// console.log(total);

// [অপশনাল]

/* ১১.একটা nested অবজেক্ট ডিক্লেয়ার করো(অর্থাৎ একটা অবজেক্ট এর প্রপার্টি এর মধ্যেও যে অবজেক্ট থাকতে পারে। আবার সেই অবজেক্ট এর প্রপার্টি এর মধ্যেও সে অবজেক্ট থাকতে পারে। সেই রকম একটা অবজেক্ট ডিক্লেয়ার করো। এবং যেকোন একটা প্রপার্টি এর মান একটা array হবে। জাস্ট এমন একটা অবজেক্ট) */
const gadgets = {
    laptopBrand: 'Asus',
    Model: 'GL552V',
    color: 'Black',
    price: 75000,
    mobile: {
        brand: 'Xiaomi',
        color: 'Steel Blue',
        price: 21000,
        etc: ['Battery', 'Charger', 'Processor']
    }
};

/* ১২.উপরের অবজেক্ট এ ডট এর আগে যে প্রশ্নবোধক চিহ্ন দিয়ে যে অপশনাল চেইনিং করা যায়। সেটা একটু প্রাকটিস করো। */
const person = {
    name: 'Ashraful',
    age: 22,
    vehicle: {
        year: 2012,
        // buyingDate: '15 January',
        drive() {
            return 'I am driving';
        },
        warranty: {
            expiryDate: 2022
        }
    }
}

const vehicleYear = person.vehicle?.year;
console.log(vehicleYear);
const warrantyExpiryDate = person.vehicle.warranty.expiryDate;
console.log(warrantyExpiryDate);
console.log(person.vehicle?.drive?.());

const buyingDate = person.vehicle?.buyingDate ?? "Don't remember the date";
console.log(buyingDate);